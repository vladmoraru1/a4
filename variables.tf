variable "access_key" {}
variable "secret_key" {}

variable "region" {
  description = "The region of the infrastucture"
  default = "eu-west-2"
}

variable "environment" {
  description = "The environment"
  default = "a4-dev"
}

variable "image_id" {
  default = "ami-017b0e29fac27906b"
}

variable "instance_type" {
  default = "t2.medium"
}

variable "key_name" {
  description = "The public key for the bastion host"
  default = "a4"
}

variable "vpc_id" {
  default = "vpc-7738c91e"
}

variable "public_subnets_id" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default = [
    "subnet-b6bfa5ce",
    "subnet-0de6f5120aa897fd2",
    "subnet-8a08f3e3"
  ]
}

variable "public_zone_id" {
  default = "Z2U00Q95U7EKEA"
}
